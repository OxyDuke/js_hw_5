function createNewUser() {
  const newUser = {
    _firstName: "",
    _lastName: "",
    
    get firstName() {
      return this._firstName;
    },

    get lastName() {
      return this._lastName;
    },
    
    setFirstName(newFirstName) {
      this._firstName = newFirstName;
    },
    setLastName(newLastName) {
      this._lastName = newLastName;
    },
    getLogin() {
      return `${this._firstName[0].toLowerCase()}${this._lastName.toLowerCase()}`;
    },
  };

  newUser.setFirstName(prompt("Indicate your firstName, please:"));
  newUser.setLastName(prompt("Indicate your lastName, please:"));

  return newUser;
}

const user = createNewUser();
console.log(user.getLogin());

user.setFirstName("New Name");
user.setLastName("New Surname");

console.log(user.getLogin());

console.log(user.firstName);
console.log(user.lastName);
